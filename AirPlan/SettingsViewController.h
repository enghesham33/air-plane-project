//
//  SettingsViewController.h
//  AirPlan
//
//  Created by MAcBookPro on 1/24/15.
//  Copyright (c) 2015 MAcBookPro. All rights reserved.
//

#import "ViewController.h"

@interface SettingsViewController : ViewController
- (IBAction)saveIp:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *ipTextField;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

@end

//
//  ViewController.m
//  AirPlan
//
//  Created by MAcBookPro on 1/19/15.
//  Copyright (c) 2015 MAcBookPro. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize alphastar,airplane,islamic,entertainment,guest,publications;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    
    [alphastar setImage:[UIImage imageNamed:@"alphastar.png"] forState:UIControlStateNormal];
    [alphastar setTitle:@"Alpha Star" forState:UIControlStateNormal];
    alphastar.titleLabel.font = [UIFont systemFontOfSize:23.0];
    alphastar.imageEdgeInsets = UIEdgeInsetsMake(5.0f, 40.0f, 0.0f, 0.0f);
    alphastar.titleEdgeInsets = UIEdgeInsetsMake(0.0f, 92.0f, 0.0f, 0.0f);
    [alphastar setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    
    
    [airplane setImage:[UIImage imageNamed:@"airplane.png"] forState:UIControlStateNormal];
    [airplane setTitle:@"Airplane" forState:UIControlStateNormal];
    airplane.titleLabel.font = [UIFont systemFontOfSize:23.0];
    airplane.imageEdgeInsets = UIEdgeInsetsMake(5.0f, 40.0f, 0.0f, 0.0f);
    airplane.titleEdgeInsets = UIEdgeInsetsMake(0.0f, 92.0f, 0.0f, 0.0f);
    [airplane setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    
    [islamic setImage:[UIImage imageNamed:@"islamic.png"] forState:UIControlStateNormal];
    [islamic setTitle:@"Islamic" forState:UIControlStateNormal];
    islamic.titleLabel.font = [UIFont systemFontOfSize:23.0];
    islamic.imageEdgeInsets = UIEdgeInsetsMake(5.0f, 40.0f, 0.0f, 0.0f);
    islamic.titleEdgeInsets = UIEdgeInsetsMake(0.0f, 92.0f, 0.0f, 0.0f);
    [islamic setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    
    [entertainment setImage:[UIImage imageNamed:@"entertainment.png"] forState:UIControlStateNormal];
    [entertainment setTitle:@"Entertainment" forState:UIControlStateNormal];
    entertainment.titleLabel.font = [UIFont systemFontOfSize:23.0];
    entertainment.imageEdgeInsets = UIEdgeInsetsMake(5.0f, 40.0f, 0.0f, 0.0f);
    entertainment.titleEdgeInsets = UIEdgeInsetsMake(0.0f, 92.0f, 0.0f, 0.0f);
    [entertainment setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    
    [guest setImage:[UIImage imageNamed:@"guest.png"] forState:UIControlStateNormal];
    [guest setTitle:@"Guest" forState:UIControlStateNormal];
    guest.titleLabel.font = [UIFont systemFontOfSize:23.0];
    guest.imageEdgeInsets = UIEdgeInsetsMake(5.0f, 40.0f, 0.0f, 0.0f);
    guest.titleEdgeInsets = UIEdgeInsetsMake(0.0f, 92.0f, 0.0f, 0.0f);
    [guest setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    
    
    [publications setImage:[UIImage imageNamed:@"publications.png"] forState:UIControlStateNormal];
    [publications setTitle:@"Publications" forState:UIControlStateNormal];
    publications.titleLabel.font = [UIFont systemFontOfSize:23.0];
    publications.imageEdgeInsets = UIEdgeInsetsMake(5.0f, 40.0f, 0.0f, 0.0f);
    publications.titleEdgeInsets = UIEdgeInsetsMake(0.0f, 92.0f, 0.0f, 0.0f);
    [publications setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];



    
    

    


    // Do any additional setup after loading the view, typically from a nib.
}

-(void)viewWillAppear:(BOOL)animated{
    [[self navigationController] setNavigationBarHidden:YES animated:YES];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

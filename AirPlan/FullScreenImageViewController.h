//
//  FullScreenImageViewController.h
//  AirPlan
//
//  Created by MAcBookPro on 1/24/15.
//  Copyright (c) 2015 MAcBookPro. All rights reserved.
//

#import "ViewController.h"

@interface FullScreenImageViewController : ViewController
@property (strong, nonatomic) IBOutlet UIImageView *fullScreenImageView;
@property NSMutableArray *images;
-(void) setImagesData:(NSMutableArray *)imageData;
-(void) setImageIndexData:(int) imageIndex;
@property int imageIndex;
@end

//
//  MoviesTableViewController.h
//  AirPlan
//
//  Created by MAcBookPro on 1/25/15.
//  Copyright (c) 2015 MAcBookPro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoviesTableViewController : UITableViewController <NSXMLParserDelegate>

@property NSURL *url;
@property NSXMLParser *parser;
@property NSMutableArray *feeds;
@property NSMutableDictionary *item;
@property NSMutableString *movieName;
@property NSMutableString *moviePath;
@property NSMutableString *movieImage;
@property NSMutableString *fullMoviePath;
@property NSString *urlString;
@property NSString *ip;
@property NSString *element;

-(void) setUrlStringData:(NSString *) urlString;

@end

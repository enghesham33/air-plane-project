//
//  GalleryCollectionViewController.h
//  AirPlan
//
//  Created by MAcBookPro on 1/24/15.
//  Copyright (c) 2015 MAcBookPro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GalleryCollectionViewController : UICollectionViewController <NSXMLParserDelegate>

@property NSMutableArray *data;
@property NSMutableArray *imagesArray;
@property NSURL *url;
@property NSXMLParser *parser;
@property NSMutableDictionary *item;
@property NSString *element;
@property NSString *ip;
@property NSMutableString *path;
@property NSMutableString *type;
@property (nonatomic,strong) UIActivityIndicatorView *indicator;
@property UIImageView *fullScreenImageView;
@property UIButton *closeButton;
@end

//
//  AirPlanViewController.m
//  AirPlan
//
//  Created by MAcBookPro on 1/23/15.
//  Copyright (c) 2015 MAcBookPro. All rights reserved.
//

#import "AirPlanViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import "GalleryViewController.h"

@interface AirPlanViewController ()
{
    BOOL parseEnded;
}
@end

@implementation AirPlanViewController

@synthesize  url ;
@synthesize  parser ;
@synthesize item ;
@synthesize element;
@synthesize path,type,ip,video;


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    
    NSString *addressFilePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"address file.plist"];
    
    
    NSMutableDictionary *dictRoot = [NSMutableDictionary dictionaryWithContentsOfFile:addressFilePath];
    
    ip = [NSString stringWithString:[dictRoot objectForKey:@"ip"]];

    url = [[NSURL alloc]initWithString:[ip stringByAppendingString:@"/service/update_section?section_id=2"]];
    
    video = [[NSMutableString alloc] init];

    dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
    dispatch_async(myQueue, ^{
        // Perform long running process
        parser = [[NSXMLParser alloc]initWithContentsOfURL:url];
        [parser setDelegate:self];
        [parser parse];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            
            
        });
    });

    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
}


- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    element = elementName;
    
    if ([element isEqualToString:@"item"]) {
        item    = [[NSMutableDictionary alloc] init];
        type   = [[NSMutableString alloc] init];
        path    = [[NSMutableString alloc] init];
        
    }
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if ([element isEqualToString:@"type"]) {
        [type appendString:string];
    } else if ([element isEqualToString:@"path"]) {
        [path appendString:string];
    }
    
}


- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    if ([elementName isEqualToString:@"item"]) {
        
        [item setObject:type forKey:@"type"];
        [item setObject:path forKey:@"path"];
        
        if (![type  isEqual: @"img"]) {
            video = path;
        }
    }
    
}

-(void)parserDidEndDocument:(NSXMLParser *)parser {
    
    
}

- (IBAction)showSafetyVideo:(id)sender {
    
    
    MPMoviePlayerViewController * controller = [[MPMoviePlayerViewController alloc]initWithContentURL:[NSURL URLWithString:[ip stringByAppendingString:video]]];
    
    [controller.moviePlayer prepareToPlay];
    [controller.moviePlayer play];
    
    // and present it
    [self presentMoviePlayerViewControllerAnimated:controller];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation gallerySegue
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//    
//    // Make sure your segue name in storyboard is the same as this line
//    if ([[segue identifier] isEqualToString:@"gallerySegue"])
//    {
//        // Get reference to the destination view controller
//        GalleryViewController *vc = [[GalleryViewController alloc]init];
//        vc =[segue destinationViewController];
//        while (feeds.count == 0) {}
//        [vc setDataArray:feeds];
//        NSLog(@"feeds.count :: %lu",(unsigned long)feeds.count);
//    }
//}


@end

//
//  QuranTableViewCell.m
//  AirPlan
//
//  Created by MAcBookPro on 1/21/15.
//  Copyright (c) 2015 MAcBookPro. All rights reserved.
//

#import "QuranTableViewCell.h"
#import <MediaPlayer/MediaPlayer.h>

@implementation QuranTableViewCell

@synthesize cellName,cellUrl,myController,moviesController,indicator;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)playPause:(id)sender {
    
    MPMoviePlayerViewController * controller = [[MPMoviePlayerViewController alloc]initWithContentURL:[NSURL URLWithString:cellUrl]];
    
    [controller.moviePlayer prepareToPlay];
    [controller.moviePlayer play];
    
    if ([indicator isEqualToString:@"qur"]) {
        
        [myController presentMoviePlayerViewControllerAnimated:controller];
        
    } else {
        
        [moviesController presentMoviePlayerViewControllerAnimated:controller];
        
    }
    // and present it
    

}
@end

//
//  ViewController.h
//  AirPlan
//
//  Created by MAcBookPro on 1/19/15.
//  Copyright (c) 2015 MAcBookPro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *alphastar;
@property (weak, nonatomic) IBOutlet UIButton *airplane;
@property (weak, nonatomic) IBOutlet UIButton *islamic;
@property (weak, nonatomic) IBOutlet UIButton *entertainment;
@property (weak, nonatomic) IBOutlet UIButton *guest;
@property (weak, nonatomic) IBOutlet UIButton *publications;

@end


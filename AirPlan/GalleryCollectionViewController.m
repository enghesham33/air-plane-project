//
//  GalleryCollectionViewController.m
//  AirPlan
//
//  Created by MAcBookPro on 1/24/15.
//  Copyright (c) 2015 MAcBookPro. All rights reserved.
//

#import "GalleryCollectionViewController.h"
#import "GalleryCollectionViewCell.h"
#import "FullScreenImageViewController.h"

@interface GalleryCollectionViewController ()

@end

@implementation GalleryCollectionViewController

static NSString * const reuseIdentifier = @"Cell";
@synthesize data,ip;
@synthesize  url ;
@synthesize  parser ;
@synthesize item ;
@synthesize element;
@synthesize path,type,imagesArray;
@synthesize indicator;
@synthesize fullScreenImageView,closeButton;

int globalIndex;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
    [self.collectionView registerClass:[GalleryCollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    
    //self.view.backgroundColor = [UIColor clearColor];
    self.collectionView.backgroundColor = [UIColor clearColor];
    [self startLoading];
    fullScreenImageView = [[UIImageView alloc]init];
    closeButton = [[UIButton alloc]init];
    closeButton =[UIButton buttonWithType:UIButtonTypeRoundedRect];

    NSString *addressFilePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"address file.plist"];
    
    
    NSMutableDictionary *dictRoot = [NSMutableDictionary dictionaryWithContentsOfFile:addressFilePath];
    
    ip = [NSString stringWithString:[dictRoot objectForKey:@"ip"]];
    
    url = [[NSURL alloc]initWithString:[ip stringByAppendingString:@"/service/update_section?section_id=2"]];

    dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
    dispatch_async(myQueue, ^{
        // Perform long running process
        
        data = [[NSMutableArray alloc]init];
        imagesArray = [[NSMutableArray alloc]init];
        
        parser = [[NSXMLParser alloc]initWithContentsOfURL:url];
        [parser setDelegate:self];
        [parser parse];
        
        for (int i=0; i<data.count; i++) {
            NSURL   *imageURL   = [NSURL URLWithString:[ip stringByAppendingString:[[data objectAtIndex:i] objectForKey:@"path"]]];
            NSData  *imageData  = [NSData dataWithContentsOfURL:imageURL];
            UIImage *image      = [UIImage imageWithData:imageData];
            [imagesArray addObject:image];
        }
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            [self stopLoading];
            [self.collectionView reloadData];
            
        });
    });

    
    // Do any additional setup after loading the view.
}

-(void) startLoading {
    indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicator.frame = CGRectMake(0.0, 0.0, 280.0, 280.0);
    indicator.center = self.view.center;
    indicator.transform = CGAffineTransformMakeScale(1.5, 1.5);
    [self.view addSubview:indicator];
    [indicator bringSubviewToFront:self.view];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    [indicator startAnimating];
}

-(void) stopLoading {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = FALSE;
    [indicator setHidden:NO];
    [indicator stopAnimating];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    element = elementName;
    
    if ([element isEqualToString:@"item"]) {
        item    = [[NSMutableDictionary alloc] init];
        type   = [[NSMutableString alloc] init];
        path    = [[NSMutableString alloc] init];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if ([element isEqualToString:@"type"]) {
        [type appendString:string];
    } else if ([element isEqualToString:@"path"]) {
        [path appendString:string];
    }
}


- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    if ([elementName isEqualToString:@"item"]) {
        
        [item setObject:type forKey:@"type"];
        [item setObject:path forKey:@"path"];
        if ([type isEqual:@"img"]) {
            [data addObject:[item copy]];
        }
    }
}


-(void)parserDidEndDocument:(NSXMLParser *)parser {
    [self.collectionView reloadData];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    
//    NSLog(@"segue function");
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
////    if ([[segue identifier] isEqualToString:@"fullScreenSegue"]){
////        FullScreenImageViewController *controller = [[FullScreenImageViewController alloc]init];
////        controller = [segue destinationViewController];
////        [controller setImageIndex:indexPath.item];
////        [controller setImages:imagesArray];
////    }
//}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.data count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    GalleryCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    UIImageView *imageView = [[UIImageView alloc] init];
    [imageView setImage:[imagesArray objectAtIndex:indexPath.item]];
    cell.backgroundView = imageView;

    return cell;
}

#pragma mark <UICollectionViewDelegate>


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    globalIndex = indexPath.item;
    NSLog(@"inside didSelectItemAtIndexPath");
    [self.collectionView setHidden:YES];
    [fullScreenImageView setHidden:NO];
    fullScreenImageView.frame = [[UIScreen mainScreen] bounds];
    fullScreenImageView.center = self.view.center;
    [fullScreenImageView setImage:[imagesArray objectAtIndex:indexPath.item]];
    [fullScreenImageView setContentMode:UIViewContentModeScaleAspectFit];
    [fullScreenImageView setBackgroundColor:[UIColor blackColor]];
    [fullScreenImageView setUserInteractionEnabled:YES];
    [self.view addSubview:fullScreenImageView];
    
    [closeButton setContentMode:UIViewContentModeTopLeft];
    [closeButton setTitle:@"Close" forState:UIControlStateNormal];
    closeButton.frame = CGRectMake(80.0, 210.0, 160.0, 40.0);
    closeButton.backgroundColor=[UIColor whiteColor];
    CGRect r = [closeButton frame];
    r.origin.y = 100.0f;
    r.origin.x=fullScreenImageView.frame.size.width - 200.0f;
    [closeButton setFrame:r];
    [fullScreenImageView addSubview:closeButton];
    [fullScreenImageView bringSubviewToFront:self.view];
    [self.collectionView setHidden:YES];
    [closeButton addTarget:self
               action:@selector(closeFullScreen)
     forControlEvents:UIControlEventAllEvents];
    
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.fullScreenImageView addGestureRecognizer:swipeleft];
    
    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    swiperight.direction=UISwipeGestureRecognizerDirectionRight;
    [self.fullScreenImageView addGestureRecognizer:swiperight];
}

-(void)swipeleft:(int)index
{
    if (globalIndex == imagesArray.count-1) {
        
    }else{
        [fullScreenImageView setImage:[imagesArray objectAtIndex:globalIndex+1]];
        globalIndex++;
    }
    
}

-(void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer
{
    if (globalIndex == 0) {
       
    }else{
        [fullScreenImageView setImage:[imagesArray objectAtIndex:globalIndex-1]];
        globalIndex--;
    }
    
}

-(void) closeFullScreen {
    [self.collectionView setHidden:NO];
    [fullScreenImageView setHidden:YES];
}

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

@end

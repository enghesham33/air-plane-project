//
//  DoaaElsafarViewController.h
//  AirPlan
//
//  Created by MAcBookPro on 1/21/15.
//  Copyright (c) 2015 MAcBookPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface DoaaElsafarViewController : UIViewController <AVAudioPlayerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *soundButton;

@property AVAudioPlayer *player;
@property NSInteger isPlay;
- (IBAction)playPauseAudio:(id)sender;

@end

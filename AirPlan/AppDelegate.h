//
//  AppDelegate.h
//  AirPlan
//
//  Created by MAcBookPro on 1/19/15.
//  Copyright (c) 2015 MAcBookPro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


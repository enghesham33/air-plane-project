//
//  DoaaElsafarViewController.m
//  AirPlan
//
//  Created by MAcBookPro on 1/21/15.
//  Copyright (c) 2015 MAcBookPro. All rights reserved.
//

#import "DoaaElsafarViewController.h"

@interface DoaaElsafarViewController ()

@end

@implementation DoaaElsafarViewController
@synthesize player,isPlay,soundButton;


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    isPlay = 0;
    // Do any additional setup after loading the view.
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/doaa-al-safar.mp3", [[NSBundle mainBundle] resourcePath]]];
    NSError *error;
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    player.numberOfLoops = 0;
    player.delegate = self;
    if (player == nil){
        NSLog(@"error in play file :: %@",[error description]);
    }
    else{
        [player play];
        isPlay = 2;
    }

    

}

- (IBAction)playPauseAudio:(id)sender {
    
    if(isPlay == 0){
        NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/doaa-al-safar.mp3", [[NSBundle mainBundle] resourcePath]]];
        NSError *error;
        player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
        player.numberOfLoops = 1;
        player.delegate = self;
        if (player == nil){
            NSLog(@"error in play file :: %@",[error description]);
        }
        else{
            //mute
           [soundButton setImage:[UIImage imageNamed:@"doaa-icons-on.png"] forState:UIControlStateNormal];
            [player play];
            isPlay = 2;
        }
    } else if(isPlay == 1) {
        //mute
        [soundButton setImage:[UIImage imageNamed:@"doaa-icons-on.png"] forState:UIControlStateNormal];
        [player setVolume:2.0];
        isPlay = 2;
    } else {
        //unmute
        [soundButton setImage:[UIImage imageNamed:@"doaa-icons-off.png"] forState:UIControlStateNormal];
        isPlay=1;
        [player setVolume:0.0];
    }
    
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *) player successfully:(BOOL)flag
{
    
    isPlay=0;
    
}

-(void)viewWillAppear:(BOOL)animated{
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end

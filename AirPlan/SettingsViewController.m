//
//  SettingsViewController.m
//  AirPlan
//
//  Created by MAcBookPro on 1/24/15.
//  Copyright (c) 2015 MAcBookPro. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController
@synthesize ipTextField,messageLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
     self.view.backgroundColor = [UIColor clearColor];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated{
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)saveIp:(id)sender {
    
    
    NSString *path = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"address file.plist"];
    
    
    NSMutableDictionary *dictRoot = [NSMutableDictionary dictionaryWithContentsOfFile:path];
    
    NSLog(@"ip adrress: %@",[dictRoot objectForKey:@"ip"]);
    NSString *ipText = ipTextField.text;
    [dictRoot setValue:ipText forKey:@"ip"];
    NSLog(@"ip adrress: %@",[dictRoot objectForKey:@"ip"]);
    
    BOOL success = [dictRoot writeToFile:path atomically:YES];
    
    if (success) {
        
        messageLabel.text=@"Ip address saved successfully";
        ipTextField.text=@"";
        
    } else {
        
        messageLabel.text=@"Failed to save ip address";
        
    }
    
    
}
@end

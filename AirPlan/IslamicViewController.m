//
//  IslamicViewController.m
//  AirPlan
//
//  Created by MAcBookPro on 1/19/15.
//  Copyright (c) 2015 MAcBookPro. All rights reserved.
//

#import "IslamicViewController.h"

@interface IslamicViewController ()

@end

@implementation IslamicViewController
@synthesize quranbutton,doaaElsafarbuttob;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    
    [quranbutton setImage:[UIImage imageNamed:@"quran.png"] forState:UIControlStateNormal];
    [quranbutton setTitle:@"Quran" forState:UIControlStateNormal];
    quranbutton.titleLabel.font = [UIFont systemFontOfSize:23.0];
    quranbutton.imageEdgeInsets = UIEdgeInsetsMake(5.0f, 40.0f, 0.0f, 0.0f);
    quranbutton.titleEdgeInsets = UIEdgeInsetsMake(0.0f, 92.0f, 0.0f, 0.0f);
    [quranbutton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    
    [doaaElsafarbuttob setImage:[UIImage imageNamed:@"Doaa.png"] forState:UIControlStateNormal];
    [doaaElsafarbuttob setTitle:@"Doaa" forState:UIControlStateNormal];
    doaaElsafarbuttob.titleLabel.font = [UIFont systemFontOfSize:23.0];
    doaaElsafarbuttob.imageEdgeInsets = UIEdgeInsetsMake(5.0f, 40.0f, 0.0f, 0.0f);
    doaaElsafarbuttob.titleEdgeInsets = UIEdgeInsetsMake(0.0f, 92.0f, 0.0f, 0.0f);
    [doaaElsafarbuttob setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

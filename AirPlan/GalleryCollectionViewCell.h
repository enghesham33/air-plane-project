//
//  GalleryCollectionViewCell.h
//  AirPlan
//
//  Created by MAcBookPro on 1/23/15.
//  Copyright (c) 2015 MAcBookPro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GalleryCollectionViewCell : UICollectionViewCell


@property (weak, nonatomic) IBOutlet UIImageView *galleryImageView;


@end

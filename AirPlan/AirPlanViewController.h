//
//  AirPlanViewController.h
//  AirPlan
//
//  Created by MAcBookPro on 1/23/15.
//  Copyright (c) 2015 MAcBookPro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AirPlanViewController : UIViewController <NSXMLParserDelegate>


@property NSURL *url;
@property NSXMLParser *parser;
@property NSMutableString *video;
@property NSMutableDictionary *item;
@property NSString *element;
@property NSString *ip;
@property NSMutableString *path;
@property NSMutableString *type;

- (IBAction)showSafetyVideo:(id)sender;

@end

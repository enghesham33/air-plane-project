//
//  FullScreenImageViewController.m
//  AirPlan
//
//  Created by MAcBookPro on 1/24/15.
//  Copyright (c) 2015 MAcBookPro. All rights reserved.
//

#import "FullScreenImageViewController.h"

@interface FullScreenImageViewController ()

@end

@implementation FullScreenImageViewController

@synthesize fullScreenImageView,images,imageIndex;

- (void)viewDidLoad {
    [super viewDidLoad];
    fullScreenImageView = [[UIImageView alloc] init];
    [fullScreenImageView setImage:[images objectAtIndex:imageIndex]];
    
    // Do any additional setup after loading the view.
}

-(void) setImagesData:(NSMutableArray *)imageData{
    images = imageData;
    NSLog(@"number of images :: %lu",(unsigned long)images.count);
}

-(void) setImageIndexData:(int)imageIndexData{
    imageIndex = imageIndexData;
     NSLog(@"index :: %lu",(unsigned long)imageIndex);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

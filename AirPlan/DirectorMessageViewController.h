//
//  DirectorMessageViewController.h
//  AirPlan
//
//  Created by MAcBookPro on 1/22/15.
//  Copyright (c) 2015 MAcBookPro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DirectorMessageViewController : UIViewController <NSXMLParserDelegate,UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *directorMessageWebView;

@property NSURL *url;
@property NSXMLParser *parser;
@property NSMutableArray *feeds;
@property NSMutableDictionary *item;
@property NSString *element;
@property NSMutableString *directorMessageHTMLString;
@property NSString *ip;


@end

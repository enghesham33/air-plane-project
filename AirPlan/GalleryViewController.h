//
//  GalleryViewController.h
//  AirPlan
//
//  Created by MAcBookPro on 1/23/15.
//  Copyright (c) 2015 MAcBookPro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GalleryViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, NSXMLParserDelegate> 

@property (weak, nonatomic) IBOutlet UICollectionView *galleryCollectionView;

@property NSMutableArray *data;
@property NSURL *url;
@property NSXMLParser *parser;
@property NSMutableDictionary *item;
@property NSString *element;
@property NSString *ip;
@property NSMutableString *path;
@property NSMutableString *type;

@end

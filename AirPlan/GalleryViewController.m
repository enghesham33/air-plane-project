//
//  GalleryViewController.m
//  AirPlan
//
//  Created by MAcBookPro on 1/23/15.
//  Copyright (c) 2015 MAcBookPro. All rights reserved.
//

#import "GalleryViewController.h"
#import "GalleryCollectionViewCell.h"

@interface GalleryViewController ()

@end

@implementation GalleryViewController

@synthesize galleryCollectionView;
@synthesize data,ip;
@synthesize  url ;
@synthesize  parser ;
@synthesize item ;
@synthesize element;
@synthesize path,type;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    
    NSString *addressFilePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"address file.plist"];
    
    
    NSMutableDictionary *dictRoot = [NSMutableDictionary dictionaryWithContentsOfFile:addressFilePath];
    
    ip = [NSString stringWithString:[dictRoot objectForKey:@"ip"]];
    
//    [self.galleryCollectionView registerClass:[GalleryCollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
    
    
    url = [[NSURL alloc]initWithString:[ip stringByAppendingString:@"/service/update_section?section_id=2"]];
    dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
    dispatch_async(myQueue, ^{
        // Perform long running process
        data = [[NSMutableArray alloc]init];
        [self.galleryCollectionView setDelegate:self];

        parser = [[NSXMLParser alloc]initWithContentsOfURL:url];
        [parser setDelegate:self];
        [parser parse];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            [galleryCollectionView reloadData];
            
            
        });
    });
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    element = elementName;
    
    if ([element isEqualToString:@"item"]) {
        item    = [[NSMutableDictionary alloc] init];
        type   = [[NSMutableString alloc] init];
        path    = [[NSMutableString alloc] init];
        
    }
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if ([element isEqualToString:@"type"]) {
        [type appendString:string];
    } else if ([element isEqualToString:@"path"]) {
        [path appendString:string];
    }

    
}


- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    if ([elementName isEqualToString:@"item"]) {
        
        [item setObject:type forKey:@"type"];
        [item setObject:path forKey:@"path"];
        if ([type isEqual:@"img"]) {
            [data addObject:[item copy]];
        }
    }
}


-(void)parserDidEndDocument:(NSXMLParser *)parser {
    
 NSLog(@"[self.data count] parserDidEndDocument :: %lu",(unsigned long)[self.data count]);
    
    [self.galleryCollectionView reloadData];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    NSLog(@"[self.data count] :: %lu",(unsigned long)[self.data count]);
    return [self.data count];
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    GalleryCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[ip stringByAppendingString:[data objectAtIndex:indexPath.row]]]]];

    
    cell.galleryImageView.image = image;
    
//    cell.galleryImageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[ip stringByAppendingString:[data objectAtIndex:indexPath.row]]]]];
    
    NSLog(@"%@",[NSURL URLWithString:[ip stringByAppendingString:[data objectAtIndex:indexPath.row]]]);
    
    return cell;
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

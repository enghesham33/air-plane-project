//
//  MoviesTableViewController.m
//  AirPlan
//
//  Created by MAcBookPro on 1/25/15.
//  Copyright (c) 2015 MAcBookPro. All rights reserved.
//

#import "MoviesTableViewController.h"
#import "QuranTableViewCell.h"

@interface MoviesTableViewController ()

@end

@implementation MoviesTableViewController

@synthesize movieImage ;
@synthesize movieName ;
@synthesize moviePath ;
@synthesize  url ;
@synthesize  parser ;
@synthesize feeds ;
@synthesize item ;
@synthesize element,ip;
@synthesize urlString;
@synthesize fullMoviePath;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    NSString *addressFilePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"address file.plist"];
    
    
    NSMutableDictionary *dictRoot = [NSMutableDictionary dictionaryWithContentsOfFile:addressFilePath];
    
    ip = [NSString stringWithString:[dictRoot objectForKey:@"ip"]];
    
    url = [[NSURL alloc]initWithString:[ip stringByAppendingString:urlString]];
    
    NSLog(@"url from movies :: %@",url);
    
    dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
    dispatch_async(myQueue, ^{
        // Perform long running process
        
        
        feeds = [[NSMutableArray alloc] init];
        parser = [[NSXMLParser alloc]initWithContentsOfURL:url];
        [parser setDelegate:self];
        [parser parse];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            // Update the UI
            [self.tableView reloadData];
            
        });
    });

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
}

-(void) setUrlStringData:(NSString *) urlData {
    urlString = urlData;
}


- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    element = elementName;
    
    if ([element isEqualToString:@"item"]) {
        
        item    = [[NSMutableDictionary alloc] init];
        movieName   = [[NSMutableString alloc] init];
        movieImage    = [[NSMutableString alloc] init];
        moviePath    = [[NSMutableString alloc] init];
        
    }
    
}



- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if ([element isEqualToString:@"movie_name"]) {
        [movieName appendString:string];
    } else if ([element isEqualToString:@"path"]) {
        [moviePath appendString:string];
    } else if ([element isEqualToString:@"movie_image"]){
        [movieImage appendString:string];
    }
    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    if ([elementName isEqualToString:@"item"]) {
        
        [item setObject:movieName forKey:@"movie_name"];
        [item setObject:moviePath forKey:@"path"];
        [item setObject:movieImage forKey:@"movie_image"];
        
        [feeds addObject:[item copy]];
        
    }
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return feeds.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    QuranTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    // Configure the cell...
    cell.cellName.text = [[feeds objectAtIndex:indexPath.row] objectForKey: @"movie_name"];
    cell.backgroundColor = [UIColor clearColor];
    cell.cellName.textColor = [UIColor whiteColor];
    fullMoviePath = [[NSMutableString alloc]initWithString:ip];
    [fullMoviePath appendString:[[feeds objectAtIndex:indexPath.row] objectForKey: @"path"]];
    cell.cellUrl = fullMoviePath;
    cell.indicator = @"mov";
    cell.moviesController = self;
    
    return cell;

}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

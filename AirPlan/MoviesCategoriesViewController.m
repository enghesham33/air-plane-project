//
//  MoviesCategoriesViewController.m
//  AirPlan
//
//  Created by MAcBookPro on 1/25/15.
//  Copyright (c) 2015 MAcBookPro. All rights reserved.
//

#import "MoviesCategoriesViewController.h"
#import "MoviesTableViewController.h"

@interface MoviesCategoriesViewController ()

@end

@implementation MoviesCategoriesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    MoviesTableViewController *vc = [[MoviesTableViewController alloc]init];
    vc =[segue destinationViewController];
    
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"arabicMovies"])
    {
        [vc setUrlString:@"/service/update_section?section_id=5&type=movie&lang=ar"];
        
    }else if([[segue identifier] isEqualToString:@"westernMovies"]){
        
        [vc setUrlString:@"/service/update_section?section_id=5&type=movie&lang=en"];
        //englishAlbums
    }else if([[segue identifier] isEqualToString:@"kidsMovies"]){
        
        [vc setUrlString:@"/service/update_section?section_id=5&type=movie&lang=en"];
        //englishAlbums
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
